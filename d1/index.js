
let x = 1397;
let y = 7831;


let sum = x + y;
let sub = x -y ;
let multi = x * y;
let div = x / y; 


console.log("result of addition operator: " + sum);

console.log("result of sub operator: " + sub);
console.log("result of multi operator: " + multi);
console.log("result of division operator: " + div);

let a = 8;

a += 2;
a += 2;
console.log(a);

// Sub/Multi/Div Ass Op

a -= 2;

console.log(a);
 a *= 2;
console.log("result of division operator: " + a);
// MDAS
let mdas = 1 + 2 - 3 * 4 / 5;
//3 * 4 = 12
// 12/5 = 2.4


console.log(mdas)


let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

// Increment and decrement


let z = 1;

let increment = ++z;
// pre-inc
console.log(increment);//2

console.log(z);//2

increment0 = z++;
// post
console.log(increment0);//2

console.log(z);//3

let b = 2;
let increment1 = ++b;
console.log(increment1);
console.log(increment1 = ++b);
// pre-dec
let dec = --z;
console.log(dec);//2
console.log(z);//2

// post-dec
dec = z--;
console.log(dec);//2
console.log(z);//1

// type coercion

let A = '10';
let B = 12;
// coer
let Ab= A + B;
console.log(Ab);//1012
console.log(typeof Ab);//string


let d = 16;
let e = 14;
let noncoer = d + e;

console.log(noncoer);
console.log(typeof noncoer);

// true is 1
// false is 0

let f = true + 1;
let g = false + 1;

console.log(f, g);
console.log(typeof g);


// Comparison Operators

let h = 'h';// ass oper
let i = "ia";

// equality operator 

console.log(1==1);//t
console.log(1==2);//false
console.log(1=='1');//true
console.log(0==false);//true
console.log('h'=='h');//true
console.log('h'==h);//true

//Strict equal opp
console.log(1===1);//t
console.log(1===2);//f
console.log(1==='1');//f
console.log(0===false);//f
console.log('h'==='h');//t
console.log('h'===h);//t
console.log('i'===i);//f

// iniquality operator

console.log(1!=1);//f
console.log(1!=2);//t
console.log(1!='1');//f
console.log(0!=false);//f
console.log('h'!='h');//f
console.log('h'!=h);//f


// stric inequality oper

console.log(1!==1);//f
console.log(1!==2);//t
console.log(1!=='1');//t
console.log(0!==false);//t
console.log('h'!=='h');//f
console.log('h'!==h);//f
console.log('i'!==i);//t

// relational opperators

let j = 50;
let k = 65;

let l = j>k;
let m = j<k;
let n = j >= k;
let o =  j <=k;

console.log(l, m, n, o); //f / t / f / t

let p = '30';
console.log(j>p); //t
console.log(k<=p); //f


// logical

let legal = true;
let regis = false;
let you = true;
// logical and oper(&&)
 console.log(legal && regis);//f
 console.log(you && legal); //t
//logical or oper(||)
console.log( legal || regis);//t
console.log(you || legal);//t
//logica not oper(!)
let legal2 = !regis;//t

//sample 

let q = 2300;
let qp = q%5;

console.log(qp);

let isqp = qp === 0;

console.log(isqp);